# Terms and Conditions

The Typeform Extractor for Keboola is built and offered by Martin Fiser (Fisa) as a third party component. It is provided as-is, without guarantees and support, and for no additional charge. 
You are free to use it or check out the [source code](https://bitbucket.org/VFisa/kbc_typeform_extractor). Please contribute to the code and report any encountered bugs.
API call is processed through KBC platform, so no sensitive information is being sent non-standard way, maintaining all Keboola recommended security standards along the way.

### Contact
Fisa    
Martin Fiser  
Vancouver, Canada (PST time)   
email: VFisa.stuff@gmail.com   
# README #

This repository is a collection of configurations needed to register Keboola Generic Extractor as a branded Typeform KBC Extractor.

### API documentation ###
[Typeform API documentation](https://www.typeform.com/help/data-api/)    

### Who do I talk to? ###

* Email: Fisa AT Keboola.com
* Private: VFisa.stuff AT gmail.com
* Twitter: VFisa
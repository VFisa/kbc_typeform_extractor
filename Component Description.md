### Component Configuration ###

Extract data from Typeform API. Please contact your administrator to obtain token for API communication.   
*Note:* Answers are returned as JSON encoded array due to not-so-ideal API definition. Please use custom app to convert JSON field into separate table structure.
Extractor extracts and produces those tables:    

#### forms (list of forms available)
 - id
 - name

#### form (basic form statistics)
 - form_id
 - stats_responses_showing
 - stats_responses_total
 - stats_responses_completed

#### questions (list of questions)
 - id
 - question
 - fieldId
 - group
 - form_pk (form_id)

#### responses (submitted information)
 - id (response token)
 - completed
 - metadata_browser
 - metadata_platform
 - metadata_date_land
 - metadata_date_submit
 - metadata_user_agent
 - metadata_referer
 - metadata_network_id
 - answers (JSON encoded array of answers)
 - form_pk (form_id)
 